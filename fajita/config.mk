PRODUCT_SOONG_NAMESPACES += \
    vendor/oneplus/apps/fajita

PRODUCT_COPY_FILES += \
    vendor/oneplus/apps/fajita/proprietary/system/etc/permissions/privapp-permissions-oem-system.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-oem-system.xml \
    vendor/oneplus/apps/fajita/proprietary/system/etc/sysconfig/hiddenapi-package-whitelist-oneplus.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/hiddenapi-package-whitelist-oneplus.xml

PRODUCT_PACKAGES += \
    OnePlusCamera \
    OnePlusCameraService \
    OnePlusGallery
